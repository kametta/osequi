import Head from "next/head";

const Home = () => {
  return (
    <>
      <Head>
        <title>Os Equi</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <p>Os Equi</p>
    </>
  );
};

export default Home;
